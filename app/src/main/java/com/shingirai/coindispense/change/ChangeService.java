package com.shingirai.coindispense.change;

import android.os.AsyncTask;

import com.shingirai.coindispense.model.CurrencyBreakdownRequest;
import com.shingirai.coindispense.model.CurrencyBreakdownResponse;

import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

public class ChangeService extends AsyncTask<CurrencyBreakdownRequest,Void,CurrencyBreakdownResponse> {

    @Override
    protected CurrencyBreakdownResponse doInBackground(CurrencyBreakdownRequest... currencyBreakdownRequests) {
        return requestBreakdown(currencyBreakdownRequests[0]);
    }

    private CurrencyBreakdownResponse requestBreakdown(CurrencyBreakdownRequest currencyBreakdownRequest){
        final String url = "http://10.0.2.2:9045/change/breakdown";
        final RestTemplate restTemplate = new RestTemplate();
        try{
            return restTemplate.postForEntity(new URI(url), currencyBreakdownRequest, CurrencyBreakdownResponse.class).getBody();
        }
        catch (URISyntaxException ex){
            return null;
        }
    }
}
