package com.shingirai.coindispense;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.shingirai.coindispense.auth.LoginService;
import com.shingirai.coindispense.model.AuthStatus;
import com.shingirai.coindispense.model.AuthenticationResponse;

import org.apache.commons.lang3.StringUtils;


public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final TextView passTextView = findViewById(R.id.password);
        final TextView userNameTextview = findViewById(R.id.email);
        final Button loginButton = findViewById(R.id.email_sign_in_button);
        loginButton.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        String userName = userNameTextview.getEditableText().toString();
                        String password = passTextView.getEditableText().toString();
                        if(StringUtils.isBlank(userName)){
                            userNameTextview.setText("Username cannot be blank");
                            return;
                        }
                        LoginService loginService = new LoginService();
                        loginService.execute(userName,password);
                        try{
                            AuthenticationResponse authenticationResponse = loginService.get();
                            if(authenticationResponse.getAuthStatus().equals(AuthStatus.AUTHENTICATED)){
                                Intent intent = new Intent(view.getContext(), MainActivity.class);
                                intent.putExtra("token",authenticationResponse.getToken());
                                startActivity(intent);
                            }
                            else{
                                userNameTextview.setError("Login Failed, check your credentials");
                            }
                        }
                        catch (Exception ex){
                        }
                    }
                }
        );
    }


}

