package com.shingirai.coindispense.model;

import java.io.Serializable;
import java.util.List;

public class CurrencyBreakdownResponse implements Serializable {

    private List<RandCurrency> currencyList;

    public List<RandCurrency> getCurrencyList() {
        return currencyList;
    }
}
