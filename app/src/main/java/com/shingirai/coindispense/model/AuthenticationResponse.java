package com.shingirai.coindispense.model;

import java.io.Serializable;

public class AuthenticationResponse implements Serializable {

    private String token;
    private AuthStatus authStatus;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public AuthStatus getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(AuthStatus authStatus) {
        this.authStatus = authStatus;
    }
}
