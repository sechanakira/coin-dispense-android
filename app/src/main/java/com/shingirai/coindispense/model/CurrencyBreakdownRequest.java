package com.shingirai.coindispense.model;

import java.io.Serializable;

public class CurrencyBreakdownRequest implements Serializable {

    private double amount;
    private String securityToken;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getSecurityToken() {
        return securityToken;
    }

    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }
}
