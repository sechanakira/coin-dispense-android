package com.shingirai.coindispense.model;

public enum RandCurrency {

    ONE_HUNDRED(100, CurrencyType.NOTE),
    FIFTY(50, CurrencyType.NOTE),
    TWENTY(20, CurrencyType.NOTE),
    TEN(10, CurrencyType.NOTE),
    FIVE(5, CurrencyType.COIN),
    TWO(2, CurrencyType.COIN),
    ONE(1, CurrencyType.COIN),
    FIFTY_CENTS(0.50, CurrencyType.COIN),
    TWENTY_FIVE_CENTS(0.20, CurrencyType.COIN),
    TEN_CENTS(0.10, CurrencyType.COIN),
    FIVE_CENTS(0.05, CurrencyType.COIN);

    private double value;
    private CurrencyType currencyType;

    RandCurrency(double value, CurrencyType currencyType){
        this.value = value;
        this.currencyType = currencyType;
    }

    public double getValue() {
        return value;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

}
