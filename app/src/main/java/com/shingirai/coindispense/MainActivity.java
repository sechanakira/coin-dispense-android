package com.shingirai.coindispense;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.shingirai.coindispense.change.ChangeService;
import com.shingirai.coindispense.model.CurrencyBreakdownRequest;
import com.shingirai.coindispense.model.CurrencyBreakdownResponse;
import com.shingirai.coindispense.model.RandCurrency;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String authToken = getIntent().getStringExtra("token");
        Button submitButton = findViewById(R.id.submit_button);
        final TextView amountTextView = findViewById(R.id.amount);
        final TextView resultTextView = findViewById(R.id.result);
        submitButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CurrencyBreakdownRequest currencyBreakdownRequest = new CurrencyBreakdownRequest();
                        String amount = amountTextView.getEditableText().toString();
                        if(!StringUtils.isBlank(amount) && StringUtils.isNumericSpace(amount.replace("."," "))){
                            currencyBreakdownRequest.setSecurityToken(authToken);
                            currencyBreakdownRequest.setAmount(Double.parseDouble(amount));
                            ChangeService changeService = new ChangeService();
                            try{
                                StringBuilder displayResult = new StringBuilder();
                                Map<RandCurrency,Integer> map = new TreeMap<>();
                                CurrencyBreakdownResponse currencyBreakdownResponse = changeService.execute(currencyBreakdownRequest).get();
                                List<RandCurrency> breakdown = currencyBreakdownResponse.getCurrencyList();
                                for(RandCurrency denom : breakdown){
                                    map.put(denom, Collections.frequency(breakdown,denom));
                                }
                                for(RandCurrency denom :map.keySet()){
                                    displayResult.append(map.get(denom) + " X " + "R" + denom.getValue() + "\n");
                                }
                                resultTextView.setText(displayResult.toString());
                            }
                            catch(Exception ex){
                            }
                        }
                        else{
                            amountTextView.setError("Amount must be numeric");
                        }
                    }
                }
        );
    }

}
