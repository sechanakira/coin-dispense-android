package com.shingirai.coindispense.auth;

import android.os.AsyncTask;

import com.shingirai.coindispense.model.AuthenticationRequest;
import com.shingirai.coindispense.model.AuthenticationResponse;

import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginService extends AsyncTask<String,String, AuthenticationResponse> {

    private AuthenticationResponse login(String userName, String password){
        final String url = "http://10.0.2.2:9045/login/auth";
        final String md5Pass = md5(password);
        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setUserName(userName);
        authenticationRequest.setPassword(md5Pass);
        RestTemplate restTemplate = new RestTemplate();
        try{
            return restTemplate.postForEntity(new URI(url), authenticationRequest, AuthenticationResponse.class).getBody();
        }
        catch (URISyntaxException ex){
            return null;
        }
    }

    @Override
    protected AuthenticationResponse doInBackground(String... strings) {
        return login(strings[0],strings[1]);
    }

    private String md5(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            StringBuilder hexString = new StringBuilder();
            for (int i=0;i<byteData.length;i++) {
                String hex=Integer.toHexString(0xff & byteData[i]);
                if(hex.length()==1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
        }
        return "";
    }
}
