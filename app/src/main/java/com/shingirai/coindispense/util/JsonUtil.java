package com.shingirai.coindispense.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shingirai.coindispense.model.AuthenticationRequest;
import com.shingirai.coindispense.model.CurrencyBreakdownRequest;

import java.io.IOException;

/**
 * Created by shingirai on 2017/10/31.
 */

public class JsonUtil {

    private JsonUtil(){
    }

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static <T> T readJSONObject(String json){
        try{
            if(json.contains("userName")){
                return objectMapper.readValue(json, (Class<T>) AuthenticationRequest.class);
            }
            else if(json.contains("token")){
                return objectMapper.readValue(json, (Class<T>) CurrencyBreakdownRequest.class);
            }
        }
        catch (IOException ioe){
        }
        return null;
    }

    public static <T> String writeJSON(T t){
        try{
            return objectMapper.writeValueAsString(t);
        }
        catch (JsonProcessingException jpe){
        }
        return "";
    }

}

